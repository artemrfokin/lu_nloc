create table if not exists credit_account
(
    id        bigserial primary key,
    user_id   integer        not null,
    loan_body numeric(17, 2) not null,
    open_date timestamp      not null,
    constraint credit_account_user_id_fkey foreign key (user_id) references users (id)
);

create table if not exists credit_account_details
(
    id             bigserial primary key,
    current_debt   numeric(17, 2) not null,
    interest       numeric(5, 2)  not null,
    latest_accrual timestamp      not null,
    account_id     bigint,
    constraint credit_account_details_account_id_fkey foreign key (account_id) references credit_account (id)
);



