create table if not exists account
(
    id       bigserial primary key,
    user_id   integer   not null,
    open_date timestamp not null,
    balance   numeric(17, 2) default 0.00,
    constraint account_user_id_fkey foreign key (user_id) references users(id)
);

insert into account(user_id, open_date, balance)
values (1, current_timestamp, 1000.00),
       (2, current_timestamp, 0.00);