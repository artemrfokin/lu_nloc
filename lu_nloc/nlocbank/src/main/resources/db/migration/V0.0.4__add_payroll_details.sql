create table if not exists payroll_details (
    id bigserial primary key,
    salary numeric(10, 2) not null,
    debit_account_id bigint not null,
    user_id integer not null,
    last_accrual timestamp not null,
    active boolean default true,
    constraint payroll_details_user_id_fkey foreign key (user_id) references users(id),
    constraint payroll_details_debit_account_id_fkey foreign key (debit_account_id) references account(id)
);