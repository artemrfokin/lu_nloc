create table if not exists users
(
    id       serial primary key,
    login    varchar(30)  not null unique,
    password varchar(100) not null
);

insert into users (login, password)
values ('user', 'user_password'),
       ('admin', 'admin_password');


create table if not exists user_info(
    user_id integer primary key,
    name varchar(30) not null,
    last_name varchar(30) not null,
    patronymic varchar(30),
    phone varchar(30),
    email varchar(50),
    constraint user_info_user_id_fkey foreign key (user_id) references users(id)
);

insert into user_info
values
(1, 'Ivan', 'Ivanov', 'Ivanovich', '+79211111111', 'ivanivanov@gmail.com'),
(2, 'Petr', 'Petrov', 'Petrovich', '+79212222222', 'petrpetrov@gmail.com');


create table if not exists auth_session(
    sid varchar(100) primary key,
    user_id integer not null unique,
    expired_date timestamp not null,
    constraint auth_session_user_id_fkey foreign key (user_id) references users(id)
);