$(document).ready(() => {

    let updater;
    clearInterval(updater);
    updater = setInterval(function(){
        updateBalances();
    }, 500)

    $('#transfer_btn').on('click', makeTransfer);

});

function updateBalances() {
    const transferRequest = {
        debitAccountId: $('.account_header').attr('account_id'),
        creditAccountDetailsId: $('#credit_account').attr('creditAccountDetailsId'),
        amount: 0.00
    };

    $.ajax({
        url: '/accounts/updateBalances',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(transferRequest)
    }).done((resp) => {
        renderNewData(resp);
    }).fail((resp) => {
        console.log('fail')
        console.log('resp is ' + resp)
    })
}

function renderNewData(response) {
    $('#account_balance').text(`${response.debitAccountBalance}`);
    $('#credit_account_balance').text(`${response.creditAccountBalance}`);
}

function makeTransfer() {
    const amount = $('#trasfer_amount').val();

    if (amount) {
        const transferRequest = {
            debitAccountId: $('.account_header').attr('account_id'),
            creditAccountDetailsId: $('#credit_account').attr('creditAccountDetailsId'),
            amount: amount
        };

        $.ajax({
            url: '/accounts/repayCreditAccount',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(transferRequest)
        }).done((resp) => {
            renderNewData(resp);
        }).fail((resp) => {
            console.log('fail')
            console.log('resp is ' + resp)
        })
    }
}