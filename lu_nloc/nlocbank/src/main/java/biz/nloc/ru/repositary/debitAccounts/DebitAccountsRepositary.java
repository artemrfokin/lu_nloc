package biz.nloc.ru.repositary.debitAccounts;

import biz.nloc.ru.domain.entities.DebitAccountEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface DebitAccountsRepositary extends CrudRepository<DebitAccountEntity, Long> {
    Collection<DebitAccountEntity> findAllByUserId(Integer id);
}
