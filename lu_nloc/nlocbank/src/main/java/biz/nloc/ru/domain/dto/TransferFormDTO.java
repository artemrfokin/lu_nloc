package biz.nloc.ru.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TransferFormDTO {
    private Long accountIdFrom;
    private Long accountIdTo;
    private Double amount;
}
