package biz.nloc.ru.domain.dto.repayment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.function.DoubleUnaryOperator;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RepaymentResponseDTO {
    private Long debitAccountId;
    private Double debitAccountBalance;
    private Long creditAccountDetailsId;
    private Double creditAccountBalance;
}
