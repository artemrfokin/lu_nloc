package biz.nloc.ru.service.authorization;


import biz.nloc.ru.domain.dto.SessionDTO;
import biz.nloc.ru.domain.dto.UserDTO;

public interface AuthorizationService {
    /** Method creates new session or updates current one.
     *  - If there is session for user with provided login, then just update session expire date.
     *  - If there is NO session for user with provided login, then new session is created.
     *  @param login - user's login
     *  @return current session of user */
    SessionDTO createOrUpdateSession(String login);

    /** Method checks whether session with provided session id is expired or not.
     * @param sid - session id */
    boolean isExpired(String sid);

    /** Removes session
     * @param sid - session id */
    void removeSession(String sid);

    /** Method searches user by session id
     * @param sid - session id */
    UserDTO findUserById(String sid);
}
