package biz.nloc.ru.service.authorization;

import biz.nloc.ru.domain.dto.SessionDTO;
import biz.nloc.ru.domain.dto.UserDTO;
import biz.nloc.ru.domain.entities.SessionEntity;
import biz.nloc.ru.domain.entities.UserEntity;
import biz.nloc.ru.exception.BankException;
import biz.nloc.ru.repositary.SessionRepositary;
import biz.nloc.ru.repositary.UserRepositary;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {
    private final SessionRepositary sessionRepositary;
    private final UserRepositary userRepositary;
    private final ModelMapper modelMapper;

    @Autowired
    public AuthorizationServiceImpl(SessionRepositary sessionRepositary, UserRepositary userRepositary, ModelMapper modelMapper) {
        this.sessionRepositary = sessionRepositary;
        this.userRepositary = userRepositary;
        this.modelMapper = modelMapper;
    }


    @Override
    public SessionDTO createOrUpdateSession(String login) {
        Optional<UserEntity> optUser = userRepositary.findByLogin(login);
        if (!optUser.isPresent()){
            throw BankException.userWithSuchLoginNotExist(login);
        }

        Optional<SessionEntity> optSession = sessionRepositary.findByLogin(login);
        LocalDateTime expiredDate = LocalDateTime.now().plusDays(1);

        if (optSession.isPresent()){
            SessionEntity sessionEntity = optSession.get();
            sessionEntity.setExpiredDate(expiredDate);
            sessionRepositary.save(sessionEntity);
            return new SessionDTO(sessionEntity.getSid(), optUser.get().getLogin(), expiredDate);
        }

        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setUser(optUser.get());
        sessionEntity.setExpiredDate(expiredDate);
        sessionRepositary.save(sessionEntity);
        return new SessionDTO(sessionEntity.getSid(), optUser.get().getLogin(), expiredDate);
    }


    @Override
    public boolean isExpired(String sid) {
        Optional<SessionEntity> optSession = sessionRepositary.findById(sid);
        return optSession
                .map(session -> session.getExpiredDate().isBefore(LocalDateTime.now()))
                .orElse(true);
    }


    @Override
    public void removeSession(String sid) {
        sessionRepositary.deleteById(sid);
    }


    @Override
    public UserDTO findUserById(String sid) {
        Optional<SessionEntity> optSessionEntity = sessionRepositary.findById(sid);
        if (optSessionEntity.isPresent()){
            return modelMapper.map(optSessionEntity.get().getUser(), UserDTO.class);
        }
        // Wrong session id
        throw new RuntimeException();
    }
}
