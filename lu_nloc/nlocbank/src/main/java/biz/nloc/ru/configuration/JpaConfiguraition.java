package biz.nloc.ru.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationVersion;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:database.properties")
public class JpaConfiguraition {
    @Value("${bank.db.host}")
    private String host;
    @Value("${bank.db.port}")
    private String port;
    @Value("${bank.db.username}")
    private String username;
    @Value("${bank.db.password}")
    private String password;
    @Value("${bank.db.name}")
    private String name;
    @Value("${bank.db.version}")
    private String version;


    @Bean
    public DataSource dataSource(){
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setJdbcUrl(String.format("jdbc:postgresql://%s:%s/%s", host, port, name));
        return dataSource;
    }


    @Bean(initMethod = "migrate")
    public Flyway flyway(DataSource dataSource){
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setTarget(MigrationVersion.fromVersion(version));
        return flyway;
    }
}
