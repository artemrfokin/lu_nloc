package biz.nloc.ru.exception;

public class BankException extends RuntimeException {
    public BankException(String message) {
        super(message);
    }

    public BankException(String message, Throwable cause) {
        super(message, cause);
    }

    /***************************    User exceptions     ******************************************/
    public static BankException userWithSuchIdNotExist(Integer userId) {
        return new BankException(String.format("User with id = %d doesn't exist.", userId));
    }

    public static BankException userWithSuchLoginNotExist(String login) {
        return new BankException(String.format("User with login \"%s\" doesn't exist.", login));
    }


    /***************************    Credit account exceptions    *********************************/
    public static BankException creditAccountWithSuchIdNotExist(Long id) {
        return new BankException(String.format("Credit account details with id = %d doesn't exist.", id));
    }

    /***************************    Debit account exceptions    *********************************/
    public static BankException debitAccountWithSuchIdNotExist(Long id) {
        return new BankException(String.format("Debit account with id = %d doesn't exist.", id));
    }

    public static BankException notEnoughMoneyOnAccount(Long id) {
        return new BankException(String.format("Not enough money on debit account with id = %d.", id));
    }
}
