package biz.nloc.ru.listener;

import biz.nloc.ru.service.salaryTransfer.SalaryAccrualWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/** This listener is responsible for starting {@link SalaryAccrualWorker} in separate thread. */
@Component
public class SalaryAccrualListener implements ApplicationListener<ContextRefreshedEvent> {
    private final SalaryAccrualWorker salaryAccrualWorker;

    @Autowired
    public SalaryAccrualListener(SalaryAccrualWorker salaryAccrualWorker) {
        this.salaryAccrualWorker = salaryAccrualWorker;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        new Thread(salaryAccrualWorker).start();
    }
}

