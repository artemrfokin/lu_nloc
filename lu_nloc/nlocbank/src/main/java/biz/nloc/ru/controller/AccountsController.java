package biz.nloc.ru.controller;

import biz.nloc.ru.domain.dto.AccountDTO;
import biz.nloc.ru.domain.dto.CreditAccountDetailsDTO;
import biz.nloc.ru.domain.dto.OpenCreditAccountRequestDTO;
import biz.nloc.ru.domain.dto.PayrollConnectionRequestDTO;
import biz.nloc.ru.domain.dto.TransferFormDTO;
import biz.nloc.ru.domain.dto.UserDTO;
import biz.nloc.ru.domain.dto.repayment.RepaymentRequestDTO;
import biz.nloc.ru.domain.dto.repayment.RepaymentResponseDTO;
import biz.nloc.ru.service.debitAccounts.DebitAccountsService;
import biz.nloc.ru.service.authorization.AuthorizationService;
import biz.nloc.ru.service.creditAccounts.CreditAccountsService;
import biz.nloc.ru.service.repayment.RepaymentService;
import biz.nloc.ru.service.salaryTransfer.PayrollDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

/** This controller serves next operations:
 *  - Open debit and credit accounts;
 *  - Execute money transfer between debit accounts;
 *  - Connect imaginary user's payroll(salary) to debit account;
 *  - Execute repayment of user's debt on credit account. */
@Controller
@RequestMapping("/accounts")
public class AccountsController {

    private final DebitAccountsService debitAccountsService;
    private final CreditAccountsService creditAccountsService;
    private final AuthorizationService authorizationService;
    private final RepaymentService repaymentService;
    private final PayrollDetailsServiceImpl payrollDetailsServiceImpl;


    @Autowired
    public AccountsController(DebitAccountsService debitAccountsService, CreditAccountsService creditAccountsService, AuthorizationService authorizationService, RepaymentService repaymentService, PayrollDetailsServiceImpl payrollDetailsServiceImpl) {
        this.debitAccountsService = debitAccountsService;
        this.creditAccountsService = creditAccountsService;
        this.authorizationService = authorizationService;
        this.repaymentService = repaymentService;
        this.payrollDetailsServiceImpl = payrollDetailsServiceImpl;
    }

    /** Endpoint renders page with user's debit and credit accounts.
     * @param sid - session id */
    @GetMapping
    public String getAccounts(Model model,
                              @CookieValue("bank_session") String sid) {
        UserDTO user = authorizationService.findUserById(sid);
        model.addAttribute("debitAccounts", debitAccountsService.findAllAccountsByUserId(user.getId()));
        model.addAttribute("creditAccounts", creditAccountsService.findAllByUserId(user.getId()));
        return "accounts";
    }

    /** Endpoint renders page with options of activities with account.
     * @param accountId - debit account id
     * @param sessionId - session id */
    @GetMapping("/{accountId}")
    public String printAccountActivities(@PathVariable("accountId") final Long accountId,
                                         @CookieValue("bank_session") String sessionId,
                                         Model model) {
        model.addAttribute("debitAccount", debitAccountsService.findByAccountId(accountId));

        UserDTO user = authorizationService.findUserById(sessionId);
        model.addAttribute("creditAccounts", creditAccountsService.findAllByUserId(user.getId()));

        return "accountMenu";
    }


    /********************************************************
     *         Opening debit and credit accounts            *
     ********************************************************/
    /** Endpoint creates new debit account.
     * @param sId - session id */
    @PostMapping("/openDebitAccount")
    public String openDebitAccount(@CookieValue("bank_session") final String sId) {
        UserDTO userDTO = authorizationService.findUserById(sId);
        debitAccountsService.createAccount(userDTO.getId());
        return "redirect:/accounts";
    }

    /** Endpoint renders page with loan application. */
    @GetMapping("/openCreditAccount")
    public String openCreditAccount(Model model) {
        model.addAttribute("openAccountRequest", new OpenCreditAccountRequestDTO());
        return "openCreditAccount";
    }

    /** Endpoint creates new credit account.
     * @param sId - session id
     * @param requestDTO - object with parameters of new credit account */
    @PostMapping("/openCreditAccount")
    public String openCreditAccount(@CookieValue("bank_session") final String sId,
                                    OpenCreditAccountRequestDTO requestDTO) {
        if (requestDTO.getLoanBody() == null){
            return "redirect:/accounts/openCreditAccount";
        }

        UserDTO userDTO = authorizationService.findUserById(sId);
        creditAccountsService.createAccount(userDTO.getId(), requestDTO);
        return "redirect:/accounts";
    }


    /********************************************************
    *   Executing money transfers between users's accounts  *
    *********************************************************/
    /** Endpoint renders transfer page.
     * @param accountIdFrom - debit account from which will be done transfer */
    @GetMapping("/{accountId}/transferToOtherUser")
    public String printTransferPage(@PathVariable("accountId") final Long accountIdFrom,
                                    Model model) {
        model.addAttribute("debitAccount", debitAccountsService.findByAccountId(accountIdFrom));

        TransferFormDTO transferFormDTO = new TransferFormDTO();
        transferFormDTO.setAccountIdFrom(accountIdFrom);
        model.addAttribute("transferForm", transferFormDTO);

        return "transfer";
    }

    /** Endpoint executes transfer from one account to another.
     * @param accountIdFrom - debit account from (source)
     * @param transferFormDTO - debit account to (destination)*/
    @PostMapping("/{accountId}/transferToOtherUser")
    public String executeTransferToOtherUser(@PathVariable("accountId") final Long accountIdFrom,
                                             @ModelAttribute("trasferForm") TransferFormDTO transferFormDTO,
                                             BindingResult bindingResult,
                                             Model model) {
        AccountDTO accountFrom = debitAccountsService.findByAccountId(accountIdFrom);
        model.addAttribute("debitAccount", accountFrom);
        model.addAttribute("transferForm", new TransferFormDTO());

        if (transferFormDTO.getAccountIdTo() == null){
            bindingResult.rejectValue("accountIdTo", "");
            return "transfer";
        }

        if (transferFormDTO.getAmount() == null){
            return "transfer";
        }

        AccountDTO accountTo = debitAccountsService.findByAccountId(transferFormDTO.getAccountIdTo());

        if (accountFrom.getId() == accountTo.getId()){
            return "transfer";
        }

        boolean isEnoughFunds = debitAccountsService.isEnoughFunds(accountIdFrom, transferFormDTO.getAmount());

        if (!isEnoughFunds) {
            bindingResult.rejectValue("amount", "");
            return "transfer";
        }

        debitAccountsService.transferMoney(accountIdFrom,
                transferFormDTO.getAccountIdTo(),
                transferFormDTO.getAmount());

        return "redirect:/accounts/" + accountFrom.getId();
    }


    /********************************************************
     *  Executing repayment user's debt on credit account  *
     ********************************************************/
    /** Endpoint renders page for credit account repayment.
     * @param debitAccountId - debit account from which will be done withdrawal (source)
     * @param creditAccountId - credit account to (destination)*/
    @GetMapping("/{debitAccountId}/repayCreditAccount/{creditAccountId}")
    public String showRepaymentPage(@PathVariable("debitAccountId") final Long debitAccountId,
                                    @PathVariable("creditAccountId") final Long creditAccountId,
                                    @CookieValue("bank_session") String sid,
                                    Model model) {
        model.addAttribute("debitAccount", debitAccountsService.findByAccountId(debitAccountId));
        CreditAccountDetailsDTO creditAccountDetailsDTO = creditAccountsService.findById(creditAccountId);
        model.addAttribute("creditAccountDetails", creditAccountDetailsDTO);

        return "repaymentCreditAccount";
    }

    /** Endpoint executes repayment of credit account.
     * @param repaymentRequestDTO - request with parameters of repayment */
    @ResponseBody
    @PostMapping("/repayCreditAccount")
    public RepaymentResponseDTO repayCreditAccount(@RequestBody RepaymentRequestDTO repaymentRequestDTO) {
        RepaymentResponseDTO RepaymentResponseDTO = repaymentService.makeRepayment(repaymentRequestDTO);
        return RepaymentResponseDTO;
    }


    /**  Endpoint updates balances of user's debit and credit accounts.
    * @param repaymentRequestDTO - object with ids of required accounts */
    @ResponseBody
    @PostMapping("/updateBalances")
    public RepaymentResponseDTO updateBalances(@RequestBody RepaymentRequestDTO repaymentRequestDTO) {
        AccountDTO debitAccountDTO = debitAccountsService.findByAccountId(repaymentRequestDTO.getDebitAccountId());
        CreditAccountDetailsDTO detailsDTO = creditAccountsService.findById(repaymentRequestDTO.getCreditAccountDetailsId());

        RepaymentResponseDTO repaymentResponseDTO = new RepaymentResponseDTO();
        repaymentResponseDTO.setDebitAccountBalance(debitAccountDTO.getBalance());
        repaymentResponseDTO.setCreditAccountBalance(detailsDTO.getCurrentDept());

        return repaymentResponseDTO;
    }


    /********************************************************
     *      Connection payroll(salary) to debit account     *
     ********************************************************/
    /** Endpoint renders page for connection payroll to debit account.
     * @param debAccountId - debit account id (destination for payroll) */
    @GetMapping("/{accountId}/connectPayroll")
    public String showConnectPayrollForm(@PathVariable("accountId") final Long debAccountId,
                                         Model model) {
        model.addAttribute("debitAccount", debitAccountsService.findByAccountId(debAccountId));
        model.addAttribute("payrollRequest", new PayrollConnectionRequestDTO());
        return "connectPayroll";
    }

    /** Endpoint connect payroll to debit account.
     * @param debAccountId - debit account id (destination for payroll)
     * @param sId - session id
     * @param requestDTO - object with payroll parameters */
    @PostMapping("/{accountId}/connectPayroll")
    public String connectPayroll(@PathVariable("accountId") final Long debAccountId,
                                 @CookieValue("bank_session") final String sId,
                                 @ModelAttribute("payrollRequest") PayrollConnectionRequestDTO requestDTO,
                                 BindingResult bindingResult,
                                 Model model) {
        boolean result = payrollDetailsServiceImpl.createPayrollDetails(debAccountId,
                requestDTO.getSalary(),
                sId);

        if (!result){
            bindingResult.rejectValue("salary", "");
            model.addAttribute("debitAccount", debitAccountsService.findByAccountId(debAccountId));
            return "connectPayroll";
        }

        return "redirect:/accounts";
    }
}

