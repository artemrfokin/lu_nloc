package biz.nloc.ru.service.creditAccounts.accrualing;

import biz.nloc.ru.domain.entities.CreditAccountDetailsEntity;
import biz.nloc.ru.exception.BankException;
import biz.nloc.ru.repositary.creditAccounts.CreditAccountDetailsRepositary;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * This worker is responsible for accruing interest on credit accounts. It works in separate thread.
 * It serves maximum 3 credit accounts (artificial maximum capacity).
 * In case list of credit accounts related to this worker is empty, worker stops itself.
 */
@Slf4j
public class AccruingWorker implements Runnable {
    private final CreditAccountDetailsRepositary creditAccountDetailsRepositary;
    /**
     * Credit accounts are served by this worker.
     */
    private CopyOnWriteArrayList<CreditAccountDetailsEntity> accountsList;
    /**
     * Credit accounts with debt equals 0.
     */
    private Collection<CreditAccountDetailsEntity> deleteList;
    private volatile boolean accountListIsNotEmpty;
    public volatile boolean isStoped;
    public volatile boolean stoppingDone;
    private Lock lock;


    public AccruingWorker(CreditAccountDetailsRepositary creditAccountDetailsRepositary,
                          CopyOnWriteArrayList<CreditAccountDetailsEntity> accountsList) {
        this.creditAccountDetailsRepositary = creditAccountDetailsRepositary;
        this.accountsList = accountsList;
        this.deleteList = new ArrayList<>();
        this.accountListIsNotEmpty = true;
        this.isStoped = false;
        this.stoppingDone = false;
        this.lock = new ReentrantLock(true);
    }


    @Override
    public void run() {
        log.info("New worker started in separate thread.");
        log.info("Accounts number = {}. Account ids: {}", accountsList.size(), getAccountIds(accountsList));

        while (accountListIsNotEmpty) {
            if (!isStoped) {
                accountsList.stream()
                        .filter(entity -> entity.getLatestAccrual().plusSeconds(3).isBefore(LocalDateTime.now()))
                        .forEach(this::accrual);

                if (deleteList.size() != 0) {
                    deleteRepayedAccounts();
                }

                if (getAccountsListSize() != 0) {
                    try {
                        Thread.sleep(10_000);
                    } catch (InterruptedException e) {
                        log.error("Thread {} was interrupted.", Thread.currentThread().getName());
                        e.printStackTrace();
                    }
                } else {
                    accountListIsNotEmpty = false;
                }

            } else {
                if (!stoppingDone) {
                    stoppingDone = true;
                }
            }
        }

        log.info("Worker finished.");
    }


    private void accrual(CreditAccountDetailsEntity detailsInCache) {
        lock.lock();
        try {
            Optional<CreditAccountDetailsEntity> optNewEntity = creditAccountDetailsRepositary.findById(detailsInCache.getId());
            if (optNewEntity.isPresent()) {
                CreditAccountDetailsEntity detailsInDB = optNewEntity.get();
                Double currentDept = detailsInDB.getCurrentDept();
                if (currentDept != 0) {
                    detailsInCache.setCurrentDept(currentDept * (1 + detailsInCache.getInterest() / 100));
                    detailsInCache.setLatestAccrual(LocalDateTime.now());
                    creditAccountDetailsRepositary.save(detailsInCache);
                } else {
                    deleteList.add(detailsInCache);
                }
            } else {
                throw BankException.creditAccountWithSuchIdNotExist(detailsInCache.getId());
            }
        } finally {
            lock.unlock();
        }
    }


    private void deleteRepayedAccounts() {
        for (CreditAccountDetailsEntity entity : deleteList) {
            accountsList.remove(entity);
        }
        deleteList.clear();
    }


    public synchronized int getAccountsListSize() {
        return accountsList.size();
    }

    public void addAccount(CreditAccountDetailsEntity entity) {
        accountsList.add(entity);
    }

    public List<CreditAccountDetailsEntity> getAccountsList() {
        return new ArrayList<>(accountsList);
    }

    public void removeAccount(CreditAccountDetailsEntity entity) {
        accountsList.remove(entity);
    }

    private String getAccountIds(CopyOnWriteArrayList<CreditAccountDetailsEntity> accountsList) {
        return accountsList.stream()
                .map(CreditAccountDetailsEntity::getId)
                .map(String::valueOf)
                .collect(Collectors.joining(", "));
    }
}
