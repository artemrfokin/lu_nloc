package biz.nloc.ru.repositary.creditAccounts;

import biz.nloc.ru.domain.entities.CreditAccountDetailsEntity;
import biz.nloc.ru.domain.entities.CreditAccountEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface CreditAccountsRepositary extends CrudRepository<CreditAccountEntity, Long> {
}
