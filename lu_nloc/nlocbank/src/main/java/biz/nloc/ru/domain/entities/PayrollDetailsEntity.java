package biz.nloc.ru.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "payroll_details")
public class PayrollDetailsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double salary;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "debit_account_id")
    private DebitAccountEntity debitAccount;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "last_accrual")
    private LocalDateTime lastAccrual;

    private Boolean active;


    public PayrollDetailsEntity(Double salary,
                                DebitAccountEntity debitAccount,
                                UserEntity user){
        this.salary = salary;
        this.debitAccount = debitAccount;
        this.user = user;
        this.lastAccrual = LocalDateTime.now();
        this.active = true;
    }
}
