package biz.nloc.ru.controller;

import biz.nloc.ru.domain.dto.AuthorisationRequestDTO;
import biz.nloc.ru.domain.dto.SessionDTO;
import biz.nloc.ru.service.authorization.AuthorizationService;
import biz.nloc.ru.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/** This controller serves Login and Logout actions. */
@Controller
public class AuthorisationController {
    private final UserService userService;
    private final AuthorizationService authService;

    @Autowired
    public AuthorisationController(UserService userService, AuthorizationService authService) {
        this.userService = userService;
        this.authService = authService;
    }


    @GetMapping("/login")
    public String auth(Model model){
        model.addAttribute("request", new AuthorisationRequestDTO());
        return "login";
    }

    /**  When user tries to login here executed checking whether the user registered before or not.
     *  - If yes, algorithm generates new session id and redirect to home page with available accounts;
     *  - If no, the user is returned on login page.
     *  @param request - object with authentication parameters */
    @PostMapping("/login")
    public String autenticate(@ModelAttribute("request") AuthorisationRequestDTO request,
                              BindingResult bindingResult,
                              HttpServletResponse response){

        boolean result = userService.authenticate(request.getLogin(), request.getPassword());

        if (!result){
            bindingResult.rejectValue("login", "");
            return "login";
        }

        SessionDTO session = authService.createOrUpdateSession(request.getLogin());
        response.addCookie(new Cookie("bank_session", session.getSid()));
        return "redirect:/accounts";
    }

    /** Endpoint logout user. When user tries to logout it executes several actions:
     *  - Current user's session is deleted.
     *  - Custom coockie with name 'bank_session' is set as expired.
     *  @param sid - session id */
    @GetMapping("/logout")
    public String logout(@CookieValue("bank_session") final String sid,
                         HttpServletRequest request,
                         HttpServletResponse response){
        authService.removeSession(sid);

        Cookie[] cookies = request.getCookies();
        Cookie sessionCookie = Arrays.stream(cookies)
                .filter(cookie -> cookie.getName().equals("bank_session"))
                .findFirst()
                .get();

        sessionCookie.setMaxAge(0);
        response.addCookie(sessionCookie);
        return "redirect:/login";
    }
}
