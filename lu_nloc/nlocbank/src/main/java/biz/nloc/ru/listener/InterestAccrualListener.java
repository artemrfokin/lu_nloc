package biz.nloc.ru.listener;

import biz.nloc.ru.service.creditAccounts.accrualing.AccruingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class InterestAccrualListener implements ApplicationListener<ContextRefreshedEvent> {
    private final AccruingService accruingService;

    @Autowired
    public InterestAccrualListener(AccruingService accruingService) {
        this.accruingService = accruingService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        new Thread(accruingService).start();
        log.info("Method onApplicationEvent finished");
    }
}
