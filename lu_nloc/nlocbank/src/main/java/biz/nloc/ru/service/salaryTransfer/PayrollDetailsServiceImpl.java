package biz.nloc.ru.service.salaryTransfer;

import biz.nloc.ru.domain.dto.UserDTO;
import biz.nloc.ru.domain.entities.DebitAccountEntity;
import biz.nloc.ru.domain.entities.PayrollDetailsEntity;
import biz.nloc.ru.domain.entities.UserEntity;
import biz.nloc.ru.exception.BankException;
import biz.nloc.ru.repositary.PayrollDetailsRepositary;
import biz.nloc.ru.repositary.UserRepositary;
import biz.nloc.ru.repositary.debitAccounts.DebitAccountsRepositary;
import biz.nloc.ru.service.authorization.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * The service allows set fictional salary, which will be automatically accrued on selected debit account.
 */
@Service
public class PayrollDetailsServiceImpl implements PayrollDetailsService {
    private final PayrollDetailsRepositary payrollDetailsRepositary;
    private final DebitAccountsRepositary debitAccountsRepositary;
    private final AuthorizationService authService;
    private final UserRepositary userRepositary;

    @Autowired
    public PayrollDetailsServiceImpl(PayrollDetailsRepositary payrollDetailsRepositary, DebitAccountsRepositary debitAccountsRepositary, AuthorizationService authService, UserRepositary userRepositary) {
        this.payrollDetailsRepositary = payrollDetailsRepositary;
        this.debitAccountsRepositary = debitAccountsRepositary;
        this.authService = authService;
        this.userRepositary = userRepositary;
    }

    public boolean createPayrollDetails(Long debitAccountId, Double salary, String sessionId) {
        DebitAccountEntity accountEntity = debitAccountsRepositary.findById(debitAccountId)
                .orElseThrow(() -> BankException.debitAccountWithSuchIdNotExist(debitAccountId));
        UserDTO userDTO = authService.findUserById(sessionId);
        UserEntity userEntity = userRepositary.findById(userDTO.getId())
                .orElseThrow(() -> BankException.userWithSuchIdNotExist(userDTO.getId()));

        if (userEntity.getPayrollDetailsEntity() != null) {
            // At this moment according to design only one payroll can be connected to debit account.
            payrollDetailsRepositary.deleteById(userEntity.getPayrollDetailsEntity().getId());
        }

        PayrollDetailsEntity payrollDetailsEntity = new PayrollDetailsEntity(salary,
                accountEntity,
                userEntity
        );

        payrollDetailsRepositary.save(payrollDetailsEntity);
        return true;
    }
}
