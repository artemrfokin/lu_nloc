package biz.nloc.ru.service.creditAccounts;

import biz.nloc.ru.domain.dto.CreditAccountDetailsDTO;
import biz.nloc.ru.domain.dto.OpenCreditAccountRequestDTO;
import biz.nloc.ru.domain.entities.CreditAccountDetailsEntity;
import biz.nloc.ru.domain.entities.CreditAccountEntity;
import biz.nloc.ru.domain.entities.UserEntity;
import biz.nloc.ru.exception.BankException;
import biz.nloc.ru.repositary.UserRepositary;
import biz.nloc.ru.repositary.creditAccounts.CreditAccountsRepositary;
import biz.nloc.ru.repositary.debitAccounts.DebitAccountsRepositary;
import biz.nloc.ru.repositary.creditAccounts.CreditAccountDetailsRepositary;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class CreditAccountServiceImpl implements CreditAccountsService {
    private final static Double INTEREST = 10.00D;

    private final CreditAccountsRepositary creditAccountsRepositary;
    private final CreditAccountDetailsRepositary creditAccountDetailsRepositary;
    private final DebitAccountsRepositary debitAccountsRepositary;
    private final UserRepositary userRepositary;
    private final ModelMapper modelMapper;

    @Autowired
    public CreditAccountServiceImpl(CreditAccountsRepositary creditAccountsRepositary,
                                    CreditAccountDetailsRepositary creditAccountDetailsRepositary,
                                    DebitAccountsRepositary debitAccountsRepositary,
                                    UserRepositary userRepositary,
                                    ModelMapper modelMapper) {
        this.creditAccountsRepositary = creditAccountsRepositary;
        this.creditAccountDetailsRepositary = creditAccountDetailsRepositary;
        this.debitAccountsRepositary = debitAccountsRepositary;
        this.userRepositary = userRepositary;
        this.modelMapper = modelMapper;
    }


    @Override
    @Transactional
    public CreditAccountDetailsDTO createAccount(Integer userId, OpenCreditAccountRequestDTO requestDTO) {
        Optional<UserEntity> optUserEntity = userRepositary.findById(userId);
        if (optUserEntity.isPresent()) {
            CreditAccountEntity credAccountEntity = new CreditAccountEntity();

            credAccountEntity.setUser(optUserEntity.get());
            credAccountEntity.setLoanBody(requestDTO.getLoanBody());
            credAccountEntity.setOpenDate(LocalDateTime.now());

            CreditAccountDetailsEntity credAccountDetailsEntity = new CreditAccountDetailsEntity();
            credAccountEntity.setDetails(credAccountDetailsEntity);

            credAccountDetailsEntity.setCurrentDept(requestDTO.getLoanBody());
            credAccountDetailsEntity.setLatestAccrual(LocalDateTime.now());
            credAccountDetailsEntity.setAccount(credAccountEntity);
            credAccountDetailsEntity.setInterest(INTEREST);
            creditAccountsRepositary.save(credAccountEntity);
            creditAccountDetailsRepositary.save(credAccountDetailsEntity);
            return modelMapper.map(credAccountDetailsEntity, CreditAccountDetailsDTO.class);
        } else {
            throw BankException.userWithSuchIdNotExist(userId);
        }
    }


    @Override
    public Collection<CreditAccountDetailsDTO> findAll() {
        Iterable<CreditAccountDetailsEntity> creditAccountDetails = creditAccountDetailsRepositary.findAll();
        return StreamSupport.stream(creditAccountDetails.spliterator(), false)
                .map(entity -> modelMapper.map(entity, CreditAccountDetailsDTO.class))
                .collect(Collectors.toList());
    }


    @Override
    public Collection<CreditAccountDetailsDTO> findAllByUserId(Integer userId) {
        Iterable<CreditAccountDetailsEntity> creditAccountDetails =
                creditAccountDetailsRepositary.findAllByUserId(userId);

        if (creditAccountDetails.iterator().hasNext()) {
            return StreamSupport.stream(creditAccountDetails.spliterator(), false)
                    .map(entity -> modelMapper.map(entity, CreditAccountDetailsDTO.class))
                    .collect(Collectors.toList());
        }

        log.info("Credit accounts related to user with id = {} are not found.", userId);
        return new ArrayList<>();
    }


    @Override
    public CreditAccountDetailsDTO findById(Long id) {
        return creditAccountDetailsRepositary.findById(id)
                .map(entity -> modelMapper.map(entity, CreditAccountDetailsDTO.class))
                .orElseThrow(() -> BankException.creditAccountWithSuchIdNotExist(id));
    }
}
