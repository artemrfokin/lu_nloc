package biz.nloc.ru.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RegisterRequestDTO {
    private String login;
    private String password1;
    private String password2;
    private String name;
    private String lastName;
    private String patronymic;
    private String phone;
    private String email;
}
