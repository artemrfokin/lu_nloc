package biz.nloc.ru.repositary;

import biz.nloc.ru.domain.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepositary extends CrudRepository<UserEntity, Integer> {
    Optional<UserEntity> findByLogin(String login);
}
