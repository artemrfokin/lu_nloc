package biz.nloc.ru.domain.dto;

import biz.nloc.ru.domain.entities.CreditAccountEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreditAccountDetailsDTO {
    private Long id;
    private Double currentDept;
    private Double interest;
    private LocalDateTime latestAccrual;
    private CreditAccountEntity account;
}
