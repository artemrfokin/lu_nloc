package biz.nloc.ru.service.debitAccounts;

import biz.nloc.ru.domain.dto.AccountDTO;

import java.util.Collection;

public interface DebitAccountsService {

    AccountDTO createAccount(Integer userId);
    Collection<AccountDTO> findAll();

    AccountDTO findByAccountId(Long id);

    /** Method checks is enough money on debit account to transfer asked amount.
     * @param accountId - debit account id
     * @param transferAmount - money amount is needed to transfer */
    boolean isEnoughFunds(Long accountId, Double transferAmount);

    /** Method executes money transfer.
     * @param idAccountFrom - id of source account
     * @param idAccountTo - id of destination account
     * @param amount - amount to transfer
     * @return source account object with updated (after transfer) state */
    AccountDTO transferMoney(Long idAccountFrom, Long idAccountTo, Double amount);

    Collection<AccountDTO> findAllAccountsByUserId(Integer id);
}
