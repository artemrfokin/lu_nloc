package biz.nloc.ru.repositary;

import biz.nloc.ru.domain.entities.PayrollDetailsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PayrollDetailsRepositary extends CrudRepository<PayrollDetailsEntity, Long> {
}
