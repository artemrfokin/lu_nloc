package biz.nloc.ru.service.creditAccounts;

import biz.nloc.ru.domain.dto.CreditAccountDetailsDTO;
import biz.nloc.ru.domain.dto.OpenCreditAccountRequestDTO;
import biz.nloc.ru.domain.entities.CreditAccountDetailsEntity;

import java.util.Collection;

public interface CreditAccountsService {
    Collection<CreditAccountDetailsDTO> findAll();
    Collection<CreditAccountDetailsDTO> findAllByUserId(Integer userId);

    CreditAccountDetailsDTO findById(Long id);

    /** Method creates credit account. Under the hood it creates two objects:
     *  {@link biz.nloc.ru.domain.entities.CreditAccountEntity} and
     *  {@link biz.nloc.ru.domain.entities.CreditAccountDetailsEntity}
     *  For increasing speed of search in database credit account related information were decoupled on two entities.*/
    CreditAccountDetailsDTO createAccount(Integer userId, OpenCreditAccountRequestDTO requestDTO);
}
