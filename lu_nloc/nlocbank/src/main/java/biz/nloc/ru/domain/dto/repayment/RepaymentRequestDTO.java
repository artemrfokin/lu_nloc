package biz.nloc.ru.domain.dto.repayment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RepaymentRequestDTO {
    private Long debitAccountId;
    private Long creditAccountDetailsId;
    private Double amount;
}
