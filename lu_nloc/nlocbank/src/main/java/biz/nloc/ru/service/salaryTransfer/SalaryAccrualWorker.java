package biz.nloc.ru.service.salaryTransfer;

import biz.nloc.ru.domain.entities.DebitAccountEntity;
import biz.nloc.ru.domain.entities.PayrollDetailsEntity;
import biz.nloc.ru.exception.BankException;
import biz.nloc.ru.repositary.PayrollDetailsRepositary;
import biz.nloc.ru.repositary.debitAccounts.DebitAccountsRepositary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.StreamSupport;

/**
 * This worker is responsible for accrual of user's salary on debit account.
 * User has option on UI to set desirable salary. And then this salary will be accrual on his debit account
 * each 10 seconds.
 * Yes, this bank can make dreams come true. :)
 */
@Service
public class SalaryAccrualWorker implements Runnable {
    private static final int WAITING_TIME = 10_000;

    private final DebitAccountsRepositary debitAccountsRepositary;
    private final PayrollDetailsRepositary payrollDetailsRepositary;
    private final Lock lock;


    @Autowired
    public SalaryAccrualWorker(DebitAccountsRepositary debitAccountsRepositary, PayrollDetailsRepositary payrollDetailsRepositary) {
        this.debitAccountsRepositary = debitAccountsRepositary;
        this.payrollDetailsRepositary = payrollDetailsRepositary;
        this.lock = new ReentrantLock(true);
    }


    @Override
    public void run() {
        while (true) {
            Iterable<PayrollDetailsEntity> payrollDetailsEntities = payrollDetailsRepositary.findAll();
            if (payrollDetailsEntities.iterator().hasNext()) {
                StreamSupport.stream(payrollDetailsEntities.spliterator(), false)
                        .filter(PayrollDetailsEntity::getActive)
                        .forEach(this::executePayrollAccrualing);
            }

            try {
                Thread.sleep(WAITING_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    @Transactional
    private void executePayrollAccrualing(PayrollDetailsEntity payrollDetails) {
        lock.lock();
        try {
            DebitAccountEntity debitAccount = debitAccountsRepositary
                    .findById(payrollDetails.getDebitAccount().getId())
                    .orElseThrow(() -> BankException.debitAccountWithSuchIdNotExist(payrollDetails.getDebitAccount().getId()));

            debitAccount.setBalance(debitAccount.getBalance() + payrollDetails.getSalary());
            debitAccountsRepositary.save(debitAccount);
        } finally {
            lock.unlock();
        }
    }
}

