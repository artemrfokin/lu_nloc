package biz.nloc.ru.service.user;

import biz.nloc.ru.domain.dto.RegisterRequestDTO;
import biz.nloc.ru.domain.entities.UserEntity;

import java.util.Optional;

public interface UserService {
    /** Method checks whether user if authenticated or not. */
    boolean authenticate(String login, String password);

    /** Method saves user info.
     * @param user - user entity
     * @param requestDTO - object with registration parameters */
    void saveUserInfo(UserEntity user, RegisterRequestDTO requestDTO);

    /** Method searches user by login. */
    Optional<UserEntity> findByLogin(String login);

    /** Method saves basic user's attributes: login and password.
     * @param requestDTO - object with registration parameters */
    UserEntity saveEntity(RegisterRequestDTO requestDTO);
}
