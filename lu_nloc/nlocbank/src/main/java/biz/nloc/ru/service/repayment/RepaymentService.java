package biz.nloc.ru.service.repayment;

import biz.nloc.ru.domain.dto.repayment.RepaymentRequestDTO;
import biz.nloc.ru.domain.dto.repayment.RepaymentResponseDTO;

public interface RepaymentService {
    /** Method executes repayment of credit account (decreasing of user's debt).
     * @param repaymentRequestDTO - parameters of debit and credit accounts
     * @return updated info about debit and credit accounts */
    RepaymentResponseDTO makeRepayment(RepaymentRequestDTO repaymentRequestDTO);
}
