package biz.nloc.ru.service.user;

import biz.nloc.ru.domain.dto.RegisterRequestDTO;
import biz.nloc.ru.domain.entities.UserEntity;
import biz.nloc.ru.domain.entities.UserInfoEntity;
import biz.nloc.ru.repositary.UserInfoRepositary;
import biz.nloc.ru.repositary.UserRepositary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepositary userRepositary;
    private final UserInfoRepositary userInfoRepositary;

    @Autowired
    public UserServiceImpl(UserRepositary userRepositary, UserInfoRepositary userInfoRepositary) {
        this.userRepositary = userRepositary;
        this.userInfoRepositary = userInfoRepositary;
    }


    @Override
    public boolean authenticate(String login, String password) {
        Optional<UserEntity> optUser = userRepositary.findByLogin(login);
        return optUser.isPresent() && optUser.get().getPassword().equals(password);
    }

    @Override
    public void saveUserInfo(UserEntity user, RegisterRequestDTO requestDTO) {
        userInfoRepositary.save(
                new UserInfoEntity(
                        user.getId(),
                        user,
                        requestDTO.getName(),
                        requestDTO.getLastName(),
                        requestDTO.getPatronymic(),
                        requestDTO.getPhone(),
                        requestDTO.getEmail()
                )
        );
    }

    @Override
    public Optional<UserEntity> findByLogin(String login) {
        return userRepositary.findByLogin(login);
    }

    @Override
    public UserEntity saveEntity(RegisterRequestDTO requestDTO) {
        UserEntity newUser = new UserEntity();
        newUser.setLogin(requestDTO.getLogin());
        newUser.setPassword(requestDTO.getPassword1());
        return userRepositary.save(newUser);
    }
}
