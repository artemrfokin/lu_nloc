package biz.nloc.ru.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user_info")
public class UserInfoEntity {
    @Id
    private Integer id;

    @MapsId
    @OneToOne
    private UserEntity user;

    private String name;

    @Column(name = "last_name")
    private String lastName;

    private String patronymic;

    private String phone;

    private String email;
}
