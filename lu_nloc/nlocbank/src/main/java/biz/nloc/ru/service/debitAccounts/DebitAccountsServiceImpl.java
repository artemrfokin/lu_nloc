package biz.nloc.ru.service.debitAccounts;

import biz.nloc.ru.domain.dto.AccountDTO;
import biz.nloc.ru.domain.entities.DebitAccountEntity;
import biz.nloc.ru.domain.entities.UserEntity;
import biz.nloc.ru.exception.BankException;
import biz.nloc.ru.repositary.debitAccounts.DebitAccountsRepositary;
import biz.nloc.ru.repositary.UserRepositary;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class DebitAccountsServiceImpl implements DebitAccountsService {
    private final DebitAccountsRepositary debitAccountRepositary;
    private final UserRepositary userRepositary;
    private final ModelMapper modelMapper;
    private final Lock lock;

    @Autowired
    public DebitAccountsServiceImpl(DebitAccountsRepositary debitAccountRepositary, UserRepositary userRepositary, ModelMapper modelMapper) {
        this.debitAccountRepositary = debitAccountRepositary;
        this.userRepositary = userRepositary;
        this.modelMapper = modelMapper;
        this.lock = new ReentrantLock(true);
    }


    @Override
    public AccountDTO createAccount(Integer userId) {
        Optional<UserEntity> optUserEntity = userRepositary.findById(userId);
        if (optUserEntity.isPresent()){
            DebitAccountEntity debitAccountEntity = new DebitAccountEntity(optUserEntity.get());
            debitAccountRepositary.save(debitAccountEntity);
            return modelMapper.map(debitAccountEntity, AccountDTO.class);
        }

        throw BankException.userWithSuchIdNotExist(userId);
    }


    @Override
    public Collection<AccountDTO> findAll() {
        Iterable<DebitAccountEntity> allAccounts = debitAccountRepositary.findAll();
        return StreamSupport.stream(allAccounts.spliterator(), false)
            .map(accountEntity -> modelMapper.map(accountEntity, AccountDTO.class))
                .collect(Collectors.toList());
    }


    @Override
    public AccountDTO findByAccountId(Long id) {
        return debitAccountRepositary.findById(id)
                .map(entity -> modelMapper.map(entity, AccountDTO.class))
                .orElseThrow(() -> BankException.debitAccountWithSuchIdNotExist(id));
    }


    @Override
    public boolean isEnoughFunds(Long accountId, Double transferAmount) {
        AccountDTO debitAccount = findByAccountId(accountId);

        return debitAccount.getBalance() >= transferAmount;
    }

    /** The method allows transfer money from one debit account to another. */
    @Override
    @Transactional
    public AccountDTO transferMoney(Long idAccountFrom, Long idAccountTo, Double amount) {
        lock.lock();
        try {
            DebitAccountEntity accountFrom = debitAccountRepositary
                    .findById(idAccountFrom)
                    .orElseThrow(() -> BankException.debitAccountWithSuchIdNotExist(idAccountFrom));
            DebitAccountEntity accountTo = debitAccountRepositary
                    .findById(idAccountTo)
                    .orElseThrow(() -> BankException.debitAccountWithSuchIdNotExist(idAccountTo));

            accountTo.setBalance(accountTo.getBalance() + amount);
            accountFrom.setBalance(accountFrom.getBalance() - amount);

            debitAccountRepositary.save(accountFrom);
            debitAccountRepositary.save(accountTo);

            return modelMapper.map(accountFrom, AccountDTO.class);
        } finally {
            lock.unlock();
        }
    }


    @Override
    public Collection<AccountDTO> findAllAccountsByUserId(Integer userId) {
        Collection<DebitAccountEntity> accountEntities = debitAccountRepositary.findAllByUserId(userId);
        if (accountEntities != null){
            return accountEntities.stream()
                    .map(entity -> modelMapper.map(entity, AccountDTO.class))
                    .collect(Collectors.toList());
        }

        log.info("Debit accounts related to user with id = {} are not found.", userId);
        return new ArrayList<>();
    }
}
