package biz.nloc.ru.interceptor;

import biz.nloc.ru.service.authorization.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Optional;

/** This interceptor checks existence of 'bank_session' cookie and its expire date. */
public class CookieSessionInterceptor implements HandlerInterceptor {
    @Autowired
    private AuthorizationService authService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Cookie[] cookies = request.getCookies();
        if (cookies == null){
            response.sendRedirect("/login");
            return false;
        }

        Optional<Cookie> possCookie = Arrays.stream(cookies)
                .filter(cookie -> cookie.getName().equals("bank_session"))
                .findFirst();

        if (!possCookie.isPresent()){
            response.sendRedirect("/login");
            return false;
        }

        if (authService.isExpired(possCookie.get().getValue())){
            response.sendRedirect("/login");
            return false;
        }

        return true;
    }
}
