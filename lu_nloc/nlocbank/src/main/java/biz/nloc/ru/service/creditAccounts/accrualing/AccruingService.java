package biz.nloc.ru.service.creditAccounts.accrualing;

import biz.nloc.ru.domain.entities.CreditAccountDetailsEntity;
import biz.nloc.ru.repositary.creditAccounts.CreditAccountDetailsRepositary;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/** This service is responsible for managing Accruing workers.
 * The service estimates available quantity of credit account and grands three accounts per each worker.
 * Each worker executes in separate thread.
 * Later the service evaluates load percentage of all workers. In case average load of one worker is less
 * than 50% of its capacity(3 accounts - fictional restriction), then the service performs relocation
 * of credit accounts between workers.
 * Empty workers is closed.
 */
@Slf4j
@Service
public class AccruingService implements Runnable {
    private final CreditAccountDetailsRepositary creditAccountDetailsRepositary;
    private final ExecutorService executorService;
    /** Set of credit accounts, which served by any worker. */
    private HashSet<Long> accountsIdsServedByAllWorkers;
    /** List of running workers. */
    private ArrayList<AccruingWorker> workersList;

    @Autowired
    public AccruingService(CreditAccountDetailsRepositary creditAccountDetailsRepositary) {
        this.creditAccountDetailsRepositary = creditAccountDetailsRepositary;
        this.executorService = Executors.newFixedThreadPool(10);
        this.accountsIdsServedByAllWorkers = new HashSet<>();
        this.workersList = new ArrayList<>();
    }

    @Override
    public void run() {
        log.info("Accruing service started.");

        long totalAccountsInWorkers = 0;

        while (true) {
            Iterable<CreditAccountDetailsEntity> accounts = creditAccountDetailsRepositary.findAll();
            log.info("Total credit accounts = {}", countAccounts(accounts));
            log.info("Total credit accounts served by workers = {}", calculateServedAccounts());
            log.info("Total accrual workers = {}", workersList.size());

            // Checking accounts which is not served by workers and which balance is not zero.
            List<CreditAccountDetailsEntity> accountsNotServedByWorkers = StreamSupport.stream(accounts.spliterator(), false)
                    .filter(entity -> entity.getCurrentDept() != 0D)
                    .filter(entity -> !accountsIdsServedByAllWorkers.contains(entity.getId()))
                    .collect(Collectors.toList());
            Integer accountsNotServedSize = accountsNotServedByWorkers.size();


            if (accountsNotServedSize != 0) {
                log.info("Accounts which is not served by workers = {}", accountsNotServedSize);
                if (workersList.size() != 0) {
                    addNotServedAccountsIntoWorkers(accountsNotServedByWorkers);
                } else {
                    createNewWorkers(accountsNotServedByWorkers);
                }
            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                log.error("Thread {} was interrupted.", Thread.currentThread().getName());
                e.printStackTrace();
            }

            updateWorkersList();

            if (workersList.size() > 1) {
                for (AccruingWorker worker : workersList) {
                    totalAccountsInWorkers += worker.getAccountsListSize();
                }

                if (totalAccountsInWorkers < workersList.size() * 3 / 2) {

                    relocateAccountsBetweenWorkers();
                    updateWorkersList();
                }
                totalAccountsInWorkers = 0;
            }
        }
    }


    private void addNotServedAccountsIntoWorkers(List<CreditAccountDetailsEntity> accountsNotServedByWorkers) {
        // Choose underloaded(number served accounts < 3) workers and add accounts to them.
        for (AccruingWorker worker : workersList) {

            int accountsInWorker = worker.getAccountsListSize();
            if (accountsInWorker < 3) {
                int freeWorkerCapacity = 3 - accountsInWorker;

                while (freeWorkerCapacity != 0 && accountsNotServedByWorkers.size() != 0) {
                    CreditAccountDetailsEntity currentAccount = accountsNotServedByWorkers.get(0);
                    worker.addAccount(currentAccount);
                    accountsIdsServedByAllWorkers.add(currentAccount.getId());
                    log.info("Credit account {} is added to worker.", currentAccount.getId());

                    accountsNotServedByWorkers.remove(currentAccount);
                    freeWorkerCapacity--;
                }
            }
        }


        if (accountsNotServedByWorkers.size() != 0) {
            log.info("Capacity of already have been created workers is used up. New workers will be created.");
            createNewWorkers(accountsNotServedByWorkers);
        }
    }

    /**
     * Method creates new workers.
     *
     * @param accountsNotServedByWorkers - new credit accounts which not served by any worker.
     */
    private void createNewWorkers(List<CreditAccountDetailsEntity> accountsNotServedByWorkers, long startIndex) {
        long accountsNumNow = accountsNotServedByWorkers.size() - startIndex;

        for (int i = (int) startIndex; i < accountsNumNow; i++) {
            Collection<CreditAccountDetailsEntity> accountsForWorker = new ArrayList<>();
            accountsForWorker.add(accountsNotServedByWorkers.get(i));

            // Check whether prepared 3 accounts or current account is last in list of not served accounts.
            if (accountsForWorker.size() == 3 || i == accountsNumNow - 1) {
                AccruingWorker accruingWorker = new AccruingWorker(creditAccountDetailsRepositary,
                        new CopyOnWriteArrayList<>(accountsForWorker));

                workersList.add(accruingWorker);
                log.info("Worker list size is {}.", workersList.size());

                executorService.submit(accruingWorker);

                accountsForWorker.stream().forEach(entity -> accountsIdsServedByAllWorkers.add(entity.getId()));
            }
        }
    }

    private void createNewWorkers(List<CreditAccountDetailsEntity> accountsNotServedByWorkers) {
        long accountsNumNow = accountsNotServedByWorkers.size();
        Collection<CreditAccountDetailsEntity> accountsForWorker = new ArrayList<>();

        for (int i = 0; i < accountsNumNow; i++) {
            accountsForWorker.add(accountsNotServedByWorkers.get(i));

            // Check whether prepared 3 accounts or current account is last in list of not served accounts.
            if (accountsForWorker.size() == 3 || i == accountsNumNow - 1) {
                AccruingWorker accruingWorker = new AccruingWorker(creditAccountDetailsRepositary,
                        new CopyOnWriteArrayList<>(accountsForWorker));

                workersList.add(accruingWorker);
                executorService.submit(accruingWorker);

                accountsForWorker.stream().forEach(entity -> accountsIdsServedByAllWorkers.add(entity.getId()));
                accountsForWorker = new ArrayList<>();
            }
        }
    }

    /** The methods relocates credit accounts between workers.
     * The aim is to make full load of workers, when in some of workers is used not maximum of their capacity.
     * Fictional worker capacity = only 3 credit accounts.
     * */
    private void relocateAccountsBetweenWorkers() {
        log.info("Relocation accounts between workers started. Worker list size = {}", workersList.size());

        workersList.stream().forEach(worker -> worker.isStoped = true);
        checkAllWorkersStopped();

        for (int wIn = 0; wIn < workersList.size(); wIn++) {
            if (wIn != workersList.size() - 1) { // not last
                AccruingWorker workerFrom = workersList.get(wIn);
                int toRelocateAccNumber = workerFrom.getAccountsListSize();

                if (toRelocateAccNumber != 3 && toRelocateAccNumber != 0) {
                    List<CreditAccountDetailsEntity> toRelocateAccounts = workerFrom.getAccountsList();

                    for (int aIn = 0; aIn < toRelocateAccounts.size(); ) {
                        for (int wIn2 = wIn + 1; wIn2 < workersList.size(); wIn2++) {
                            AccruingWorker workerTo = workersList.get(wIn2);

                            while (workerTo.getAccountsListSize() != 3 && toRelocateAccNumber != 0) {
                                CreditAccountDetailsEntity accountToTransfer = toRelocateAccounts.get(aIn);
                                workerTo.addAccount(accountToTransfer);
                                workerFrom.removeAccount(accountToTransfer);
                                aIn++;
                                toRelocateAccNumber--;
                            }
                        }
                    }
                }
            }
        }

        reRunAllWorkers();
        log.info("Relocation finished.");
    }

    /**
     * Method makes check for empty workers.
     * If worker serves zero accounts, it is removed from workerList.
     */
    public void updateWorkersList() {
        log.debug("Updating list of workers.");
        ArrayList<AccruingWorker> stoppedWorkers = new ArrayList<>();
        workersList.stream()
                .filter(worker -> worker.getAccountsListSize() == 0)
                .forEach(worker -> stoppedWorkers.add(worker));

        if (stoppedWorkers.size() != 0) {
            log.debug("Stopped workers which serves 0 accounts = {}.", stoppedWorkers.size());

            stoppedWorkers.stream()
                    .forEach(worker -> workersList.remove(worker));

            log.debug("{} stopped workers are removed from worker list.", stoppedWorkers.size());
        } else {
            log.debug("All workers in active state.");
        }
    }

    private long countAccounts(Iterable<CreditAccountDetailsEntity> accounts) {
        return StreamSupport.stream(accounts.spliterator(), false)
                .count();
    }


    private void checkAllWorkersStopped() {
        boolean allWorkersAreStoped = false;
        while (!allWorkersAreStoped) {
            long count = workersList.stream()
                    .filter(worker -> worker.stoppingDone)
                    .count();
            if (count == workersList.size()) {
                allWorkersAreStoped = true;
            }
        }
        log.debug("All workers are stopped.");
    }

    private void reRunAllWorkers() {
        workersList.stream()
                .forEach(worker -> worker.isStoped = false);
    }

    private long calculateServedAccounts() {
        if (workersList.size() != 0) {
            return workersList.stream()
                    .map(AccruingWorker::getAccountsListSize)
                    .reduce((a, b) -> a + b).orElse(0);
        }
        return 0;
    }
}
