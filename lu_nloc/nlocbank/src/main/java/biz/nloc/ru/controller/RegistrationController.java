package biz.nloc.ru.controller;

import biz.nloc.ru.domain.dto.RegisterRequestDTO;
import biz.nloc.ru.domain.dto.SessionDTO;
import biz.nloc.ru.domain.entities.UserEntity;
import biz.nloc.ru.service.authorization.AuthorizationService;
import biz.nloc.ru.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/** This controller is responsible for Registration operation. */
@Controller
@RequestMapping("/registration")
public class RegistrationController {
    private final UserService userService;
    private final AuthorizationService authService;

    @Autowired
    public RegistrationController(UserService userService, AuthorizationService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    /** Endpoint renders register page. */
    @GetMapping
    public String getRegisterForm(Model model){
        model.addAttribute("request", new RegisterRequestDTO());
        return "registration";
    }

    /** Endpoint executes registration. When user tries to register here executed
     *  checking whether the user registered before or not.
     *  - If yes, the user will be informed that such login has already been taken by other user;
     *  - If no, new user is saved and new session id is generated.
     * @param requestDTO - object with registration parameters
     */
    @PostMapping
    public String register(@ModelAttribute("request") RegisterRequestDTO requestDTO,
                           BindingResult bindingResult,
                           HttpServletResponse response){

        Optional<UserEntity> optUser = userService.findByLogin(requestDTO.getLogin());
        if (optUser.isPresent()){
            bindingResult.rejectValue("login", "");
            return "registration";
        }

        if (!requestDTO.getPassword1().equals(requestDTO.getPassword2())){
            bindingResult.rejectValue("password", "");
            return "registration";
        }

        UserEntity userEntity = userService.saveEntity(requestDTO);
        userService.saveUserInfo(userEntity, requestDTO);

        SessionDTO session = authService.createOrUpdateSession(requestDTO.getLogin());
        response.addCookie(new Cookie("bank_session", session.getSid()));
        return "redirect:/accounts";
    }
}
