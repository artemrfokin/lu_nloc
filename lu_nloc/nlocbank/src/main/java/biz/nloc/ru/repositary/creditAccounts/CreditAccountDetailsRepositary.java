package biz.nloc.ru.repositary.creditAccounts;

import biz.nloc.ru.domain.dto.CreditAccountDetailsDTO;
import biz.nloc.ru.domain.entities.CreditAccountDetailsEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CreditAccountDetailsRepositary extends CrudRepository<CreditAccountDetailsEntity, Long> {
    @Query(value = "select * from credit_account_details " +
            "join credit_account ca on credit_account_details.account_id = ca.id " +
            "join users u on ca.user_id = u.id " +
            "where u.id = :userId",
            nativeQuery = true)
    Iterable<CreditAccountDetailsEntity> findAllByUserId(@Param("userId") Integer userId);
}
