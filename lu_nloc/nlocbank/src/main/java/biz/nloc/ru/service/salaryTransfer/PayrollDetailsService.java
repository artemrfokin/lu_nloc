package biz.nloc.ru.service.salaryTransfer;

public interface PayrollDetailsService {
    /** This method creates payroll details object.
     * @param debitAccountId - debit account on which will be transferred salary
     * @param salary - desired salary amount ))
     * @param sessionId - session id */
    boolean createPayrollDetails(Long debitAccountId, Double salary, String sessionId);
}
