package biz.nloc.ru.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

/** For increasing speed of search in database credit account related information were decoupled on two entities.*/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "credit_account_details")
public class CreditAccountDetailsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "current_debt")
    private Double currentDept;

    private Double interest;

    @Column(name = "latest_accrual")
    private LocalDateTime latestAccrual;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private CreditAccountEntity account;
}
