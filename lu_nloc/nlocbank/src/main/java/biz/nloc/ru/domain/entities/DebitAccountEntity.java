package biz.nloc.ru.domain.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "account")
public class DebitAccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "open_date")
    private LocalDateTime openDate;

    private Double balance;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @OneToOne(mappedBy = "debitAccount", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private PayrollDetailsEntity payrollDetailsEntity;


    public DebitAccountEntity(UserEntity user) {
        this.openDate = LocalDateTime.now();
        this.balance = 0.00D;
        this.user = user;
    }
}
