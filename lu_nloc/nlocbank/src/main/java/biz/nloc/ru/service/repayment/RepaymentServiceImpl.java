package biz.nloc.ru.service.repayment;

import biz.nloc.ru.domain.dto.repayment.RepaymentRequestDTO;
import biz.nloc.ru.domain.dto.repayment.RepaymentResponseDTO;
import biz.nloc.ru.domain.entities.DebitAccountEntity;
import biz.nloc.ru.domain.entities.CreditAccountDetailsEntity;
import biz.nloc.ru.exception.BankException;
import biz.nloc.ru.repositary.debitAccounts.DebitAccountsRepositary;
import biz.nloc.ru.repositary.creditAccounts.CreditAccountDetailsRepositary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The service allows to reduce debt on a credit account by transferring money from an debit account.
 */
@Service
public class RepaymentServiceImpl implements RepaymentService {
    private final DebitAccountsRepositary debitAccountsRepositary;
    private final CreditAccountDetailsRepositary creditAccountDetailsRepositary;
    private final Lock lock;

    @Autowired
    public RepaymentServiceImpl(DebitAccountsRepositary debitAccountsRepositary,
                                CreditAccountDetailsRepositary creditAccountDetailsRepositary) {
        this.debitAccountsRepositary = debitAccountsRepositary;
        this.creditAccountDetailsRepositary = creditAccountDetailsRepositary;
        this.lock = new ReentrantLock();
    }


    @Override
    @Transactional
    public RepaymentResponseDTO makeRepayment(RepaymentRequestDTO request) {
        DebitAccountEntity debitAccount = debitAccountsRepositary.findById(request.getDebitAccountId())
                .orElseThrow(() -> BankException.debitAccountWithSuchIdNotExist(request.getDebitAccountId()));
        CreditAccountDetailsEntity creditAccountDetails = creditAccountDetailsRepositary.findById(request.getCreditAccountDetailsId())
                .orElseThrow(() -> BankException.creditAccountWithSuchIdNotExist(request.getCreditAccountDetailsId()));

        if (debitAccount.getBalance() < request.getAmount()) {
            throw BankException.notEnoughMoneyOnAccount(debitAccount.getId());
        }

        Double amount = request.getAmount();

        lock.lock();
        try {
            if (request.getAmount() > creditAccountDetails.getCurrentDept()) {
                amount = creditAccountDetails.getCurrentDept();
            }

            debitAccount.setBalance(debitAccount.getBalance() - amount);
            creditAccountDetails.setCurrentDept(creditAccountDetails.getCurrentDept() - amount);

            debitAccount = debitAccountsRepositary.save(debitAccount);
            creditAccountDetails = creditAccountDetailsRepositary.save(creditAccountDetails);

            return new RepaymentResponseDTO(
                    debitAccount.getId(),
                    debitAccount.getBalance(),
                    creditAccountDetails.getId(),
                    creditAccountDetails.getCurrentDept());
        } finally {
            lock.unlock();
        }
    }
}
