package biz.nloc.ru.repositary;

import biz.nloc.ru.domain.entities.SessionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SessionRepositary extends CrudRepository<SessionEntity, String> {

    @Query(value = "select * from auth_session s " +
            "join users u on s.user_id = u.id " +
            "where u.login = :login",
    nativeQuery = true)
    Optional<SessionEntity> findByLogin(@Param("login") String login);

}
