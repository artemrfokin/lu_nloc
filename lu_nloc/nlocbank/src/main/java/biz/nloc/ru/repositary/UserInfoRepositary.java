package biz.nloc.ru.repositary;

import biz.nloc.ru.domain.entities.UserEntity;
import biz.nloc.ru.domain.entities.UserInfoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoRepositary extends CrudRepository<UserInfoEntity, Integer> {
    UserInfoEntity findByUser(UserEntity userEntity);
}
